// PDP11 Assembler (c) 15-01-2015 vinxru

// Feb-2018 removed WIN32 code and translated to English by SHAOS

#include "compiler.h"
#include <iostream>

int main(int argc, char** argv)
{
    int i,j=0;
    char *po;

    printf("\nPDP11/8080 Assembler/C\n");
    printf("(c) 2015 vinxru\n");
    printf("(c) 2017 ALeksey Morozov (aleksey.f.morozov@gmail.com)\n");
    printf("MK85/8085 VERSION MODIFIED AND MAINTAINED BY SHAOS\n");
    printf("GIT-REPO: https://gitlab.com/shaos/pdp11asm85\n\n");

    try {
        Compiler c;

        // expects one argument
        if(argc < 2) {
            std::cout << "Usage: " << argv[0] << " filename.asm [options]" << std::endl;
            return -1;
        }

        // process arguments
        for(i=1;i<argc;i++) {
            if(argv[i][0]=='-') {
               if(argv[i][1]=='D') {
                  po = strchr(argv[i],'=');
                  if(po) {
                    *po=0;po++;
                    c.addMacro(&argv[i][2],po);
                  }
                  else c.addMacro(&argv[i][2]);
               }
               else
               {
                  std::cout << "Unknown argument " << argv[i] << std::endl;
                  return -2;
               }
            }
            else if(!j) j=i;
        }
        if(!j) return -3;

        // compilation
        c.compileFile(argv[j]);

        // exit without errors
        std::cout << "Done" << std::endl;
        return 0;

    } catch(std::exception& e) {
        // exit with errors
        std::cout << e.what() << std::endl;
        return 1;
    }
}
