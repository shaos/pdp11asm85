# pdp11asm85

Alexey Morozov's Assembler/C for PDP11 - KR1801VM1/2 and Intel 8080/8085 for Linux and WinXP (through CYGWIN).

Forked from https://github.com/alemorf/pdp11asm (not existing anymore) and modified by Shaos.

## license

Original author (Alexey Morozov aka vinxru aka alemorf) wrote in February 2018
that this code may be used for any purpose without any restrictions,
but he is interested to be informed about newly discovered bugs:
```
alemorf:
Пользуйтесь как вам угодно, никаких ограничений. Мне не жалко.
Максимум, что меня интересует - это про баги мне рассказать.
```
Source: http://www.nedopc.org/forum/viewtopic.php?p=143028#p143028

## changelist

January 2018

- Removed C++11 code
- Fixed `@N` for PDP-11
- Added `make_mk85_rom` with checksum calculation

February 2018

- Removed WIN32 code

April 2018

- Added some patches from Ivanq for PDP-11 assembler
- Working on converting some comments to English

June 2019

- Rollback of some change from Ivanq that broke MK85

February 2024

- Swapped `BVS` and `BVC` codes for PDP-11 assembler
- Added operators `%` and `&` (to be able to take lower byte of the label)
- Added `-D` option to define macros through command line in form of `-DNAME` or `-DNAME=255` (value is limited to a byte)

## how to compile 8080 code

This assembler is for PDP-11 by default and you need to use special directive to make it to understand Intel 8080 mnemonics:
```
.i8080
```
This assembler doesn't produce HEX. It produces BIN file stright away.
Also by default assembler doesn't generate LST file - you need to specify platform and file format to make LST along with BIN:
```
.i8080
.include "your8080code.asm"
make_radio86rk_rom "test.rkr"
```
It will generate TEST.LST (regular listing file) and TEST.RKR (binary file for Radio-86RK computer or Emu80 emulator
and that is not technically a ROM, but loadable binary with a header and a checksum).
